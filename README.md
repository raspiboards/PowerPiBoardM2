# PowerPiM2 -- Version 1.0 (preliminary)

PowerPiM2 is a printed circuit board (PCB), in a Raspberry-Pi Hat shape format, implements some of essential features missing in Raspberry Pi 2 / 3 boards.

## Features List
- __Real-Time Clock Functionality__. It is given by DS3231 real-time clock (RTC) by Dallas Semiconductors (now Maxim). PowerPiM2 also provides battery support (by mounting a CR2032 battery holder), letting RTC work when Raspberry Pi is not powered.
- __4-Ampere Power Line for Raspberry-Pi__. Two TSR 2-2450 switching DC-DC converters by Traco Power, connected in parallel configuration, provide a low-noise 5-volt rail to feed Raspberry Pi and USB peripherals in a energy-efficient way (power efficiency about 90%). 
- __Additional Power Lines__. Based on TSR 1 family by Traco Power, they provide near 1 A power lines featuring high-efficient (sometimes rounding 95%) voltage-conversion, at several voltage levels as shown below.
<table>
  <tr>
    <th>Nominal Voltage Rail</th>
    <th>Switching Supply</th>
    <th>Maximum Current Allowed</th>
  </tr>
  <tr>
    <td>12.0 V</td>
    <td>TSR 1-24120</td>
    <td>1 A</td>
  </tr>
  <tr>
    <td>3.3 V</td>
    <td>TSR 1-2433</td>
    <td>980 mA</td>
  </tr>
  <tr>
    <td>2.5 V</td>
    <td>TSR 1-2425</td>
    <td>980 mA</td>
  </tr>
  <tr>
    <td>1.8 V</td>
    <td>TSR 1-2418</td>
    <td>1 A</td>
  </tr>
</table>

- __Extremely Low-Noise Voltage References__. Featuring extremely low noise, these voltage references (based on REF50XX family by Texas Instruments) meet necessary requirements to work as voltage threshold references for analog-to-digital converters (ADC), digital-to-analog converters (DAC) and similar precision devices.
<table>
 <tr>
    <th>Voltage Reference Rail</th>
    <th>Reference IC</th>
    <th>Absolute Maximum Current</th>
  </tr>
  <tr>
    <td>3.0 V</td>
    <td>REF5030</td>
    <td>10 mA</td>
  </tr>
  <tr>
    <td>2.048 V</td>
    <td>REF5020</td>
    <td>10 mA</td>
  </tr>
</table>

## Input Power Requirements
This board needs a minimum input power of 40 watts, with an input voltage ranging from 15 to 24 volts. Most computing chargers meet these power requirements.

## Contents of this repository
- __Schematics__, schematics both in Altium Designer file format (.schdoc) and PDF format (.pdf).
- __PCB Design Files__, in Altium Designer file format (.pcbdoc).
- __Bill of Materials__, in LibreOffice Spreadsheet Format (.ods).
- __Gerber Files__ (*soon, when final version 1.0 is released*).